const coverConfig = require('../config/cover.config');

module.exports = (sequelize, Sequelize) => {
    const Video = sequelize.define("videos", {  // Table name and fields
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        title: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: {
            type: Sequelize.STRING,
            allowNull: false
        },
        video: {
            type: Sequelize.STRING,
            allowNull: true,
            defaultValue: coverConfig.DEFAULT
        },

        userId: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
        
    });

    return Video;
};

const videos = [];

 