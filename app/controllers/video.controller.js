const Video = require('../models').Video;
const path = require('path');
const child_process = require('child_process');
const encoding = require('../media/encoding');
const { User } = require('../models');
const coverConfig = require('../config/cover.config');
//const ffmpeg = require('ffmpeg');


// Retrieve all videos
module.exports.getAll = async (req, res, next) => {
    // We should query the database
    const videos = await Video.findAll();
    res.status(200).json(videos);
};

// Retrieve all Users
module.exports.getAllUsers = async (req, res, next) => {
    // We should query the database
    const user = await User.findAll();
    res.status(200).json(user);
};

// Create a new video
module.exports.create = async (req, res, next) => {
    // No validation needed
    const video = await Video.create( { title: req.body.title, description: req.body.description, userId: req.body.id_user } );
    const coverFile = req.files.videoFile;
    const extension = path.extname(coverFile.name);
    const coverFileName = coverConfig.NAME_PREFIX + video.id;
    const uploadedFileName = coverConfig.UPLOADS + coverFileName + extension;
    coverFile.mv(uploadedFileName);

    const outputFile = coverConfig.PUBLIC + coverFileName + '.mp4';
    const destination = await encoding.encodeMp4(uploadedFileName, outputFile);
    const portadas = await encoding.getThumbnail(destination);
    const calidad  =   await encoding.encodeDash("/film/"+coverFileName + ".mp4","/dash/");
    //const calidad = await encoding.encodeDash('/film/' + coverFileName + '.mp4', '/dash/');
    await video.update({ video: destination });    
    res.status(201).json(video);
};

/*module.exports.create = (data) => {
    const id = videos.length + 1;
    const video = { id: id, title: data.title, description: data.description }; 
    videos.push(video);
    return Promise.resolve(video);
};

module.exports.findAll = () => {
    return Promise.resolve(videos);    
};

module.exports.findById = (id) => {
    const video = videos.find(video => video.id == id);
    return Promise.resolve(video);
}

module.exports.drop = () => {  
    videos.length = 0;
    return Promise.resolve();
}*/

// Get an existing video
module.exports.get = async (req, res, next) => {
    // No validation needed
    const video = await Video.findByPk( req.params.id );
    if (video) {
        res.status(200).json(video);
    }
    else {
        res.status(404).end();
    }    
};

// Get an existing User
module.exports.getUserById = async (req, res, next) => {
    // No validation needed
    const user = await User.findByPk( req.params.id );
    if (user) {
        res.status(200).json(user);
    }
    else {
        res.status(404).end();
    }    
};

// Actualizar un usuario existente
module.exports.updateUser = async (req, res, next) => {
    try{
        const user=await User.findByPk(req.params.id);
        if(!user){
            res.status(404).end;
            return;
        }

        // Par metros nuevos
        const NewUsername = req.body.username;
        const NewName = req.body.name;
        const NewEmail = req.body.email;
        const NewPassword = req.body.password;

        // Usuario actualizado
        await user.update({ username: NewUsername, name: NewName, email: NewEmail, password: NewPassword });
        res.status(200).json(user);
    }
    catch(error){
        return next(error);
    }
};

// Upload film image to an existing video
module.exports.upload = async (req, res, next) => {
    try {
        const video = await Video.findByPk( req.params.id );
        if (!video) {
            res.status(404).end();
            return;
        }
    // Aniadimos el video a su titulo
    const filmFile = req.files.filmFile;
    const extension = path.extname(filmFile.name);       
    const destination = '/film/film-' + video.id + extension;
    filmFile.mv(destination);

    const destination2 = '/capturas/film-' + video.id + '.png';

    if(extension == ".mp4"){

    }else{
        encod.encodeMp4(destination);
    }

    const destination3 = '/film/film-' + video.id + '.mp4';

    //const mpd = '/film/film-' + video.id + '.mpd' + '/stream.mpd';

    encod.getThumbnail(destination2);
    encod.encodeDash(destination3);

    // Update movie
    await video.update({ capturas: destination2, film: destination3});
    res.status(200).json(video);

    }
    catch (error) {
        return next(error);
    }
};