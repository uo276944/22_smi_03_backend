const path = require('path');
const child_process = require('child_process');

exports.normalize = (imageFileName, outputFileName) => {
  return new Promise((resolve, reject) => {
    const command = `ffmpeg -y -i ${imageFileName} ${outputFileName}`;

    // Transcode
    child_process.exec(command, (err, stdout, stderr) => {
      if (err) {
        return reject(new Error(`Transcoding error. ${stderr}`));
      }
      resolve(outputFileName);
    });
  });
}

const formats = [
  ".mp4",
  ".mov",
  ".avi",
  ".wmv",
  ".mkv"
];

exports.encodeMp4 = (filmFile) => {
  return new Promise((resolve, reject) => {
      const parsedFile = path.parse(filmFile);
      const outputFile = `${parsedFile.name}.mp4`
      // /videos is the shared volume with nginx http server
      const outputFilePath = `/film/${outputFile}`;
      // ffmpeg command to convert the video to mp4 format with stardard codec
      const command = `ffmpeg -y -i ${filmFile} ${outputFilePath}`;

      // Is it the right extension?    
      if (!formats.includes(parsedFile.ext)) {
      return reject(new Error("wrong video format" ));
      }

      // Encode
      child_process.exec(command, (err, stdout, stderr) => {
      if (err) {
          return reject(new Error(`Encoding error. ${stderr}`));
      }
      resolve(outputFilePath);
      });
  });
}

exports.getThumbnail = (filmFile) => {
  return new Promise((resolve, reject) => {
    const parsedFile = path.parse(filmFile);
    const outputFile = `${parsedFile.name}.png`
    // /videos is the shared volume with nginx http server
    const outputFilePath2 = `/capturas/${outputFile}`;
    // ffmpeg command to extract a frame at the beginning of video and copy it to ${outputFilePath} 
    const command = `ffmpeg -i ${filmFile} -ss 00:00:01 -vframes 1  ${outputFilePath2}`;

    // Encode
    child_process.exec(command, (err, stdout, stderr) => {
      if (err) {
        return reject(new Error(`Encoding error. ${stderr}`));
      }
      resolve(outputFilePath2);
    });
  });
}

/*exports.encodeDash = (filmFile) => {
  return new Promise((resolve, reject) => {
    //const parsedFile = path.parse(filmFile);
    const outputFile = `${filmFile.name}.mpd`

    // /Videos is the shared volume with nginx http server
    const outputFilePath = `/film/${outputFile}`;

    // command to run a bash script for dash encoding
    const command = `/script.sh ${filmFile}`; 

    // Encode
    child_process.exec(command, (err, stdout, stderr) => {
      if (err) {
        return reject(new Error(`Encoding error. ${stderr}`));
      }
      resolve(outputFilePath);
    });

  });
}*/

exports.encodeDash = (filmFile, outputPath) => {
  return new Promise((resolve, reject) => {
    // command to run a bash script for dash encoding
    const command = `./ScriptDash.sh ${filmFile}`;

    // Encode
    child_process.exec(command, (err, stdout, stderr) => {
      if (err) {
        return reject(new Error(`Encoding error. ${stderr}`));
      }
      // command to move the representation and manifest files to /videos/dash
      // const command = `mv ${filmFile}-* /dash`;
      resolve(outputPath);
    });
  });
}